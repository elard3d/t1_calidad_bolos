﻿using Examen_T1___Simulador_de_Bolos;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimuladorBolosTest
{
    [TestFixture]
    public class UnitTest
    {
        [Test]
        public void Test1()
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(8, 1, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(9);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test2()
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(5, 4, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(9);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test3() //FALLA
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(5, 4, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(8);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test4() //FALLA
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(10, 4, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(14);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test5()
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(0, 0, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(0);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test6()
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(0, 9, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(9);

            Assert.AreEqual(resultado, resultEsperado);

        }


        [Test]
        public void Test7()
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(8, 4, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(10);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test8() //FALLA
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(4, 4, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(-8);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test9()
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(5, 4, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(9);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test10() //FALLA
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(5, 5, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(10);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test11() //FALLA
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(9, 0, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(9);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test12()
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(4, 4, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(8);

            Assert.AreEqual(resultado, resultEsperado);

        }


        [Test]
        public void Test13() //FALLA
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(0, 10, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(10);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test14()
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(1,0, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(1);

            Assert.AreEqual(resultado, resultEsperado);

        }

        [Test]
        public void Test15() //FALLA
        {
            Bowling bowling = new Bowling();

            bowling.StarBowling();
            bowling.Lanzar(6, 3, 1);

            List<int> resultado = bowling.ObtenerPuntaje(1);

            List<int> resultEsperado = new List<int>();

            resultEsperado.Add(9);

            Assert.AreEqual(resultado, resultEsperado);

        }


    }
}
