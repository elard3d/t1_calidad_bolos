﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_T1___Simulador_de_Bolos
{
    public class Bowling
    {
        public List< BowlingGame> gameStart { get; set; }
        public int total { get; set; }

        int position = 0;

        public int StarBowling()
        {
            gameStart = new List<BowlingGame>();
            return 1;
        }


        public int Lanzar(int strike1, int strike2, int player)
        {

            var play = new BowlingGame();
            play.strike1= strike1;
            play.strike2 = strike2;
            play.player = player;

            gameStart.Add(play);

            return 1;
        }


        public List<int> ObtenerPuntaje(int player)
        {
            
            List<int> puntajeAcumulado = new List<int>();

            int pos1 = 0;
            foreach (var play in gameStart)
            {
                if (play.player == player)
                {
                    if ((play.strike1 + play.strike2) < 10 && play.strike1 != 10)
                    {
                        play.puntos = play.strike1 + play.strike2;
                        puntajeAcumulado.Add(play.puntos);
                        pos1++;
                    }

                }

            }

            foreach (var play in gameStart)
            {
                if (play.player == player)
                {
                    //Aplicando regla por Strike
                    if (play.strike1== 10)
                    {
                        if (position == 0)
                        {
                            play.puntos = 10 +
                                gameStart[position + 1].strike1+ gameStart[position + 1].strike2;
                            puntajeAcumulado.Add(play.puntos);
                        }
                        else
                        {
                            if (position < 9)
                            {
                                play.puntos = 10 + gameStart[position - 1].puntos +
                                gameStart[position + 1].strike1+ gameStart[position + 1].strike2;
                                puntajeAcumulado.Add(play.puntos);
                            }
                            else
                            {
                                play.puntos = 10 + gameStart[position - 1].puntos;
                                puntajeAcumulado.Add(play.puntos);
                            }
                        }
                    }
                    else
                    {
                        //Aplicando reglas Spare
                        if (play.strike2 + play.strike1 == 10)
                        {
                            play.puntos = 10 + gameStart[position + 1].strike1;
                            puntajeAcumulado.Add(play.puntos);

                            if (play.puntos == 10 && position == 9)
                            {
                                play.puntos = 10;
                                puntajeAcumulado.Add(play.puntos);
                            }


                        }
                    }



                }
                position++;

            }


            return puntajeAcumulado;
        }

        public int SumarPuntosJugador(int jugador)
        {
            int suma = 0;
            var x = ObtenerPuntaje(jugador);

            foreach (var item in x)
            {
                suma = suma + item;

            }


            return suma;
        }

    }

}

